#!/usr/bin/perl
use strict;
use warnings;
use RRDs;
use FindBin;
#
# Contains:
# 5 sec readings for 2 hours.
# 1 min readings for 36 hours.
# 5 min readings for 10 days.
# 1 hour readings for 6 months.
#
# Author: Dennis Hedegaard <neo2k@neo2k.dk>

#exit &create_cpu_rrd("cpu");

# Gets basedir.
my $basedir = $FindBin::Bin;
$basedir =~ s/\/bin//;

die "Please supply arguments.\n" if scalar @ARGV == 0;
foreach (@ARGV) {
	&create_mem_rrd("mem") if $_ eq "mem";
	&create_cpu_rrd("cpu") if $_ eq "cpu";
	&create_swap_rrd("swap") if $_ eq "swap";
}
# &create_mem_rrd("mem");

# Creates a new rrd for memory gathering.
sub create_mem_rrd {
	my ($name) = @_ or die $!;
	if ( -f "$basedir/data/$name.rrd" ) {
		print STDERR "$basedir/data/$name.rrd already exists, removing..\n";
		unlink "$basedir/data/$name.rrd" or print STDERR "Unable to remove $basedir/data/$name.rrd: $!";
	}
	RRDs::create("$basedir/data/$name.rrd",
		"--step", "15", # step 15 sec.
		"DS:total:GAUGE:600:U:U", # total mem
		"DS:free:GAUGE:600:U:U", # free mem
		"DS:buffers:GAUGE:600:U:U", # buffers
		"DS:cached:GAUGE:600:U:U", # cache
                "RRA:AVERAGE:0.5:1:1440", # 15 sec for 5 hours.
                "RRA:AVERAGE:0.5:8:2160", # 1 min for 36 hours.
                "RRA:AVERAGE:0.5:80:2880", # 10 min for 10 days.
                "RRA:AVERAGE:0.5:480:4320"); # 1 hour for 6 months.
		my $err = RRDs::error;
		if ($err) {
			chomp $err;
			print STDERR "Unable to create mem RRD for $name: $err\n";
			return 1;
		} else {
			print "Created mem RRD for $name..\n";
			return 0;
		}
}

# Creates a new rrd, for CPU gathering, with a specific name.
# returns 1 on error, 0 on success.
sub create_cpu_rrd {
	my ($name) = @_ or die $!;
	if ( -f "$basedir/data/$name.rrd" ) {
		print STDERR "$basedir/data/$name.rrd already exists, removing it..\n";
		unlink "$basedir/data/$name.rrd" or print STDERR "Unable to remove $name.rrd: $!";
	}
	RRDs::create("$basedir/data/$name.rrd",
                "--step", "15", # stepping is 15 seconds.
                "DS:load1:GAUGE:600:U:U",
                "DS:load5:GAUGE:600:U:U",
                "DS:load15:GAUGE:600:U:U",
		"DS:cpucount:GAUGE:600:U:U",
                "RRA:AVERAGE:0.5:1:1440", # 15 sec for 5 hours.
                "RRA:AVERAGE:0.5:8:2160", # 1 min for 36 hours.
                "RRA:AVERAGE:0.5:80:2880", # 10 min for 10 days.
                "RRA:AVERAGE:0.5:480:4320"); # 1 hour for 6 months.
        my $err = RRDs::error;
        if ($err) {
                print STDERR "Unable to create RRD for $name: $err\n";
		return 1;
        } else {
                print "Created RRD for $name\n";
		return 0;
        }
}

sub create_swap_rrd {
	my ($name) = @_ or die $!;
	if ( -f "$basedir/data/$name.rrd" ) {
		print STDERR "$basedir/data/$name.rrd already exists, removing it..\n";
		unlink "$basedir/data/$name.rrd" or print STDERR "Unable to remove $name.rrd: $!";
	}
	RRDs::create("$basedir/data/$name.rrd",
		"--step", "15",
		"DS:total:GAUGE:600:U:U",
		"DS:used:GAUGE:600:U:U",
		"DS:free:GAUGE:600:U:U",
                "RRA:AVERAGE:0.5:1:1440", # 15 sec for 5 hours.
                "RRA:AVERAGE:0.5:8:2160", # 1 min for 36 hours.
                "RRA:AVERAGE:0.5:80:2880", # 10 min for 10 days.
                "RRA:AVERAGE:0.5:480:4320"); # 1 hour for 6 months.
	my $err = RRDs::error;
	if ($err) {
		chomp $err;
		print STDERR "Unable to create RRD for $name: $err\n";
		return 1;
	} else {
		print "Created RRD for $name\n";
		return 0;
	}
}
