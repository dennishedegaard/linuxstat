#!/usr/bin/perl
use strict;
use warnings;
no warnings 'redefine';
use CGI;
use RRDs;

# Buffer size used for printing png.
use constant BUFFER_SIZE => 8_192;
# should point to where linuxstat resides.
use constant BASEDIR => "/home/linuxstat/linuxstat";
# where images should be written, and read again.
use constant IMGDIR => "/tmp/linuxstat_images";
# where the project resides on the webserver
use constant WEBPATH => => "/linuxstat/index.cgi";

# The rrd used for swap.
local our $swap_rrd = BASEDIR . "/data/swap.rrd";

# get current epoch.
local our $epoch = (`date +%s`)[0];
chomp $epoch;

# query object.
local our $q = CGI->new;
local our $imgStyle = "border: 0;";

# "constants" used throughout the site
# The pagesize for memory.
local our $page_size = int `getconf PAGESIZE`;
# The number of pages shared.
local our $ksm_pages_shared = &read_ksm_pages_shared();
# The number of pages sharing.
local our $ksm_pages_sharing = &read_ksm_pages_sharing();
# end constants

# setup start "constants".
sub init {
}

# Returns the number of shared pages in KSM.
sub read_ksm_pages_shared {
	open FP, "</sys/kernel/mm/ksm/pages_shared" or die "Unable to read pages shared by KSM: $!";
	my $result = <FP>;
	chop $result;
	close FP;
	return int $result;
}

# Returns "how many more sites are sharing them i.e. how much saved"
sub read_ksm_pages_sharing {
	open FP, "</sys/kernel/mm/ksm/pages_sharing" or die "Unable to read pages sharing by KSM: $!";
	my $result = <FP>;
	chomp $result;
	close FP;
	return int $result;
}

&init();

if ($q->param("img")) {
	&generate_image($q->param("img"));
}
elsif ($q->param("type")) {
	if (! -d IMGDIR ) {
		mkdir IMGDIR;
	}
	my $type = $q->param("type");
	if ($type eq "cpu" or $type eq "mem" or $type eq "swap") {
		&refresh_type_graph($type);
		&generate_header($q->param("type"));
		&generate_type_html($type);
		&generate_footer();
	} else {
		&generate_error_html();
	}
}
else {
	# check for image dir.
	if (! -d IMGDIR ) {
		mkdir IMGDIR;
	}
	&refresh_all();
	&generate_header();
	&generate_html();
	&generate_footer();
}

sub get_css {
	my $style = <<END;
img {
	border: 0;
}
END
	return $style;
}

# Refreshes all for 6 hours back.
# Used for generating graphs for the &generate_html function.
sub refresh_all {
	my $start;
	foreach (qw/6_hours week/) {
		$start = $epoch - 60 * 60 * 6 if $_ eq "6_hours";
		$start = $epoch - 60 * 60 * 24 * 7 if $_ eq "week";
		&generate_cpugraph(BASEDIR . "/data/cpu.rrd", IMGDIR . "/cpu-$_.png", "System load ($_)", $start);
		&generate_memgraph(BASEDIR . "/data/mem.rrd", IMGDIR . "/mem-$_.png", "Memory usage ($_)", $start);
		&generate_swapgraph(BASEDIR . "/data/swap.rrd", IMGDIR . "/swap-$_.png", "Swap usage ($_)", $start);
	}
}

# generates new graphs.
# Generates last hour, last 6 hours, last week, last month and last year
# for the &generate_type_html function. For the specific $type chosen.
sub refresh_type_graph {
	my ($type) = @_ or die $!;
	# iterate images.
	foreach (qw/hour 6_hours day week month year/) {
		my $start;
		if ($_ eq "hour") {$start = $epoch - 60 * 60;}
		if ($_ eq "6_hours") {$start= $epoch - 60 * 60 *6;}
		elsif ($_ eq "day") {$start = $epoch - 60 * 60 * 24;}
		elsif ($_ eq "week") {$start = $epoch - 60* 60 * 24 * 7;}
		elsif ($_ eq "month") {$start = $epoch - 60 * 60 * 24 * 30;}
		elsif ($_ eq "year") {$start = $epoch - 60 * 60 * 24 * 365;}
		&generate_cpugraph(BASEDIR . "/data/$type.rrd", IMGDIR . "/$type-$_.png", "System load ($_)", $start) if $type eq "cpu";
		&generate_memgraph(BASEDIR . "/data/$type.rrd", IMGDIR . "/$type-$_.png", "Memory usage ($_)", $start) if $type eq "mem";
		&generate_swapgraph(BASEDIR . "/data/$type.rrd", IMGDIR . "/$type-$_.png", "Swap usage ($_)", $start) if $type eq "swap";
	}
	
}

# Generates an error html, for when a bad request is issued.
sub generate_error_html {
	print $q->header( -status => "400 Bad Request", -type => "text/html" );
}

# Generates the default header for valid pages.
sub generate_header {
	my ($title) = @_;
	print $q->header( -type => "text/html" );
	print $q->start_html( -title => "linuxstat - overview", -style => { -code => &get_css() }, -head=>$q->meta( { -http_equiv => 'REFRESH', -content => '60' } ) );
	if ($title) {
		print $q->h1("linuxstat - $title") . "\n";
	} else {
		print $q->h1("linuxstat - overview") . "\n";
	}
	print $q->a( { -href => WEBPATH }, "Overview");
	print $q->span(" | ");
	print $q->a( { -href => WEBPATH . "?type=cpu" }, "CPU");
	print $q->span(" | ");
	print $q->a( { -href => WEBPATH . "?type=mem" }, "Memory");
	print $q->span(" | ");
	print $q->a( { -href => WEBPATH . "?type=swap" }, "Swap");
	print $q->hr . "\n";
}

sub generate_footer {
	print $q->end_html();
}

# Generates default HTML for a default page.
sub generate_html {
	print $q->start_table;
	print $q->start_Tr, $q->start_td;
	print $q->a( { -href => WEBPATH . "?type=cpu" }, $q->img( { -src => $q->url . "?img=cpu-6_hours.png", -alt => "cpu-6_hours" } )) . "\n";
	print $q->end_td, $q->start_td;
	print $q->a( { -href => WEBPATH . "?type=cpu" }, $q->img( { src => $q->url . "?img=cpu-week.png", -alt => "cpu-week" } )) . "\n";
	print $q->end_td, $q->end_Tr, $q->start_Tr, $q->start_td;
	print $q->a( { -href => WEBPATH . "?type=mem" }, $q->img( { -src => $q->url . "?img=mem-6_hours.png", -alt => "mem-6_hours" } )) . "\n";
	print $q->end_td, $q->start_td;
	print $q->a( { -href => WEBPATH . "?type=mem" }, $q->img( { -src => $q->url . "?img=mem-week.png", -alt => "mem-week" } )) . "\n";
	print $q->end_td, $q->end_Tr, $q->start_Tr, $q->start_td;
	print $q->a( { -href => WEBPATH . "?type=swap" }, $q->img( { -src => $q->url . "?img=swap-6_hours.png", -alt => "swap-6_hours" } )) . "\n";
	print $q->end_td, $q->start_td;
	print $q->a( { -href => WEBPATH . "?type=swap" }, $q->img( { -src => $q->url . "?img=swap-week.png", -alt => "swap-week" } )) . "\n";
	print $q->end_td, $q->end_Tr . "\n";
	print $q->end_table;
}

# Generates HTML for cpu.
sub generate_type_html {
	my ($type) = @_ or die $!;
	print $q->start_table;
	print $q->start_Tr;
	print $q->td($q->img( { -src => $q->url . "?img=$type-hour.png", -alt => "$type-hour" } ));
	print $q->td($q->img( { -src => $q->url . "?img=$type-6_hours.png", -alt => "$type-6_hours" } ));
	print $q->end_Tr . "\n";;
	print $q->start_Tr;
	print $q->td($q->img( { -src => $q->url . "?img=$type-day.png", -alt => "$type-day" } ));
	print $q->td($q->img( { -src => $q->url . "?img=$type-week.png", -alt => "$type-week" } ));
	print $q->end_Tr . "\n";
	print $q->start_Tr;
	print $q->td($q->img( { -src => $q->url . "?img=$type-month.png", -alt => "$type-month" } ));
	print $q->td($q->img( { -src => $q->url . "?img=$type-year.png", -alt => "$type-year" } ));
	print $q->end_Tr . "\n";
	print $q->end_table;
}

# Attempts to read an image into buffer, then write it to the http body with the png type
# in the head, if the file doesn't exist or illegal characters are encountered, a 404 is returned.
sub generate_image {
	my ($image) = @_ or die $!;
	$image =~ s/\///g;
	if (open IMAGE, IMGDIR . "/$image") {
		print $q->header( -type => "image/png" );
		binmode STDOUT;
		my $read = 0;
		my $buffer;
		print $buffer while (read(IMAGE, $buffer, BUFFER_SIZE) > 0);
		close IMAGE;
	} else {
		print $q->header( -status => "404 Not Found", -type => "text/html" );
		print $q->start_html;
		print $q->h1("bad param") . "\n";
		print $q->end_html;
	}
}

# generates the CPU graph.
# It is stored in $png, data is read from $rrd. The title if $title and the start of the
# graph is $start.
sub generate_cpugraph {
	my ($rrd, $png, $title, $start) = @_ or die $!;
	RRDs::graph($png,
		"--lower-limit", 0,
		"--upper-limit", 4,
		"--title", $title,
		"-v", "CPU",
		"-s", $start,
		"-i",
		"-c", "BACK#FFFFFF",
		"-c", "SHADEA#FFFFFF",
		"-c", "SHADEB#FFFFFF",
		"TEXTALIGN:center",
		"DEF:load1=$rrd:load1:AVERAGE",
		"DEF:load5=$rrd:load5:AVERAGE",
		"DEF:load15=$rrd:load15:AVERAGE",
		"DEF:cpucount=$rrd:cpucount:AVERAGE",
		"VDEF:vcpucount=cpucount,AVERAGE",
		"VDEF:vload1=load1,LAST",
		"VDEF:vload5=load5,LAST",
		"VDEF:vload15=load15,LAST",
		"VDEF:vavg=load1,AVERAGE",
		"VDEF:vmax=load1,MAXIMUM",
		"AREA:vcpucount#00FF00: Number of CPUs",
		"LINE:load1#FF0000:Average CPU load\\j",
		"COMMENT:\\s",
		"GPRINT:vcpucount:Total number of CPUs\\: %.0lf",
		"GPRINT:vload1:Average last 1 mins\\: %.2lf\\j",
		"GPRINT:vavg:Average load\\: %.2lf",
		"GPRINT:vload5:Average last 5 mins\\: %.2lf\\j",
		"GPRINT:vmax:Maximum load\\: %.2lf",
		"GPRINT:vload15:Average last 15 mins\\: %.2lf\\j");
	my $err = RRDs::error;
	if ($err) {
		print "$err\n";
	}
}


# Generates memory graphs.
sub generate_memgraph {
	my ($rrd, $png, $title, $start) = @_ or die $!;
	# calculation based on 8k pages
	my $ksm_mb_shared = $ksm_pages_shared * $page_size / 1024 / 1024;
	# convert to string and round to 2 decimals
	$ksm_mb_shared = sprintf "%.2f", $ksm_mb_shared;
	# calculations based on 8k pages
	my $ksm_mb_sharing = $ksm_pages_sharing * $page_size / 1024 / 1024;
	# convert to a string and round to 2 decimals
	$ksm_mb_sharing = sprintf "%.2f", $ksm_mb_sharing;
	# generate the graph
	RRDs::graph($png,
		"--lower-limit", 0,
		"--title", $title,
		"-s", $start,
		"-v", "Bytes of memory",
		"-i",
		"-c", "BACK#FFFFFF",
		"-c", "SHADEA#FFFFFF",
		"-c", "SHADEB#FFFFFF",
		"DEF:total=$rrd:total:AVERAGE",
		"DEF:free=$rrd:free:AVERAGE",
		"DEF:buffers=$rrd:buffers:AVERAGE",
		"DEF:cached=$rrd:cached:AVERAGE",
		"CDEF:used=total,free,-,buffers,-,cached,-,1024,/,1000,*,1000,*",
		"CDEF:buffer_cache=buffers,cached,+,1000,*",
		"CDEF:ctotal=total,1024,/,1000,*,1000,*",
		"CDEF:cfree=free,1024,/,1000,*,1000,*",
		"VDEF:vused=used,LAST",
		"VDEF:vbuffer_cache=buffer_cache,LAST",
		"VDEF:vtotal=ctotal,LAST",
		"VDEF:vfree=cfree,LAST",
		"AREA:used#000099:used memory",
		"AREA:buffer_cache#4444EE:buffers and cache:STACK",
		"AREA:cfree#000000:free memory\\j:STACK",
		"LINE:ctotal#000000",
		"COMMENT:\\s",
		"GPRINT:vtotal:Total memory\\: %3.2lf %sB",
		"GPRINT:vfree:Free memory\\: %.2lf %sB\\j",
		"GPRINT:vbuffer_cache:Buffer and Cache\\: %3.2lf %sB",
		"GPRINT:vused:Used memory\\: %.2lf %sB\\j",
		"COMMENT:KSM shared\\: $ksm_pages_shared ($ksm_mb_shared MB)",
		"COMMENT:KSM sharing\\: $ksm_pages_sharing ($ksm_mb_sharing MB)\\j");
	my $err = RRDs::error;
	if ($err) {
		chomp $err;
		print "$err\n";
	}
}

# Generates swap graphs.
sub generate_swapgraph {
	my ($rrd, $png, $title, $start) = @_ or die $!;
	RRDs::graph($png,
		"--lower-limit", 0,
		"--title", $title,
		"-s", $start,
		"-v", "Bytes of swap",
		"-i",
		"-c", "BACK#FFFFFF",
		"-c", "SHADEA#FFFFFF",
		"-c", "SHADEB#FFFFFF",
		"DEF:swap_total=$swap_rrd:total:AVERAGE",
		"DEF:swap_used=$swap_rrd:used:AVERAGE",
		"DEF:swap_free=$swap_rrd:free:AVERAGE",
		"CDEF:cswap_total=swap_total,1024,*",
		"CDEF:cswap_used=swap_used,1024,*",
		"CDEF:cswap_free=swap_free,1024,*",
		"VDEF:vswap_total=cswap_total,LAST",
		"VDEF:vswap_used=cswap_used,LAST",
		"VDEF:vswap_free=cswap_free,LAST",
		"AREA:cswap_used#8A00C2:Used swap",
		"AREA:cswap_free#000000:Free swap\\j:STACK",
		"COMMENT:\\s",
		"GPRINT:vswap_total:Total swap\\: %.2lf %sB",
		"GPRINT:vswap_used:Used swap\\: %.2lf %sB",
		"GPRINT:vswap_free:Free swap\\: %.2lf %sB\\j");
	my $err = RRDs::error;
	if ($err) {
		chomp $err;
		print "$err\n";
	}

}
